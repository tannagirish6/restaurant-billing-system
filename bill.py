from tkinter import*
import math,random,os
from tkinter import messagebox
class Bill_App:
    a1=5    
        
    def welcome_bill(self):
           
        
        self.txtarea.delete('1.0',END)
        self.txtarea.insert(END,"\t  Kapleshwar Restaurant\n")
        self.txtarea.insert(END,f"\n Bill Number   : {self.bill_no.get()}")
        self.txtarea.insert(END,f"\n Customer Name : {self.c_name.get()}")
        self.txtarea.insert(END,f"\n Bill Number   : {self.c_phon.get()}")
        self.txtarea.insert(END,f"\n=======================================")
        self.txtarea.insert(END,f"\n Products\t\tQTY\t\tPrice")
        self.txtarea.insert(END,f"\n=======================================")

    def bill_area(self):
        if self.c_name.get()=="" or self.c_phon.get()=="":
                messagebox.showerror("Error","Customer details are must")
        elif self.Total_Juice_price.get()=="Rs. 0.0" and self.Total_Items_price.get()=="Rs. 0.0" and self.Total_Desserts_price.get()=="Rs. 0.0":
                messagebox.showerror("Error","No products selected")


        else:        
                self.welcome_bill()
                #=====================JUICES================
                if self.oj.get()!=0:
                        self.txtarea.insert(END,f"\n Orange Juice\t\t{self.oj.get()}\t\t{self.orange_juice_price}")
                if self.wj.get()!=0:
                        self.txtarea.insert(END,f"\n WaterMelon Juice\t\t{self.wj.get()}\t\t{self.watermelon_juice_price}")
                if self.Pj.get()!=0:
                        self.txtarea.insert(END,f"\n PineApple Juice\t\t{self.Pj.get()}\t\t{self.Pineapple_juice_price}")
                if self.Sj.get()!=0:
                        self.txtarea.insert(END,f"\n SweetLime Juice\t\t{self.Sj.get()}\t\t{self.SweetLime_juice_price}")
                if self.cj.get()!=0:
                        self.txtarea.insert(END,f"\n Carrot Juice\t\t{self.cj.get()}\t\t{self.Carrot_juice_price}")
                if self.poj.get()!=0:
                        self.txtarea.insert(END,f"\n Pomgranate Juice\t\t{self.poj.get()}\t\t{self.Pomgranate_juice_price}")
                #========ITEMS===========
                if self.fi.get()!=0:
                        self.txtarea.insert(END,f"\n Fries Meal\t\t{self.fi.get()}\t\t{self.Fries_meal_price}")
                if self.li.get()!=0:
                        self.txtarea.insert(END,f"\n Lunch Meal\t\t{self.li.get()}\t\t{self.Lunch_meal_price}")
                if self.bi.get()!=0:
                        self.txtarea.insert(END,f"\n Burger Meal\t\t{self.bi.get()}\t\t{self.Burger_meal_price}")
                if self.pi.get()!=0:
                        self.txtarea.insert(END,f"\n Pizza Meal\t\t{self.pi.get()}\t\t{self.Pizza_meal_price}")
                if self.cbi.get()!=0:
                        self.txtarea.insert(END,f"\n Cheese Burger  Meal\t\t{self.cbi.get()}\t\t{self.Cheese_Burger_meal_price}")
                if self.hi.get()!=0:
                        self.txtarea.insert(END,f"\n Hotdog Meal\t\t{self.hi.get()}\t\t{self.Cheese_HotDog_price}")        
                
                #=========Desserts==========
                if self.id.get()!=0:
                        self.txtarea.insert(END,f"\n Ice Cream\t\t{self.id.get()}\t\t{self.Ice_Cream}")  
                if self.gd.get()!=0:
                        self.txtarea.insert(END,f"\n Gulab Jamun\t\t{self.gd.get()}\t\t{self.Gulab_Jamun}")  
                if self.sbd.get()!=0:
                        self.txtarea.insert(END,f"\n Halwa\t\t{self.sbd.get()}\t\t{self.Brownie}") 
                if self.hd.get()!=0:
                        self.txtarea.insert(END,f"\n Sizzling Brownie\t\t{self.hd.get()}\t\t{self.Halwa1}")  
                if self.ccd.get()!=0:
                        self.txtarea.insert(END,f"\n CupCake\t\t{self.ccd.get()}\t\t{self.CupCake}")  
                if self.fcd.get()!=0:
                        self.txtarea.insert(END,f"\n Fresh Cream\t\t{self.fcd.get()}\t\t{self.Fresh_Cream}")          
                self.txtarea.insert(END,f"\n---------------------------------------")
                if self.Juice_Tax.get()!= "Rs. 0.0":
                                print()
                                #self.txtarea.insert(END,f"\n Juice Tax\t\t\t    {self.Juice_Tax.get()}")
                if self.Items_Tax.get()!= "Rs. 0.0":
                                print()

                                #self.txtarea.insert(END,f"\n Items Tax\t\t\t    {self.Items_Tax.get()}")
                if self.Desserts_Tax.get()!= "Rs. 0.0":
                                print()

                                #self.txtarea.insert(END,f"\n Desserts Tax\t\t\t    {self.Desserts_Tax.get()}")
                self.t1=self.d_tax+self.i_tax+self.j_tax
                
        
                
                self.txtarea.insert(END,f"\n CGST(2.5%)\t\t\t    {self.without_taxes*0.025}")    
                self.txtarea.insert(END,f"\n SGST(2.5%)\t\t\t    {self.without_taxes*0.025}")  
                self.txtarea.insert(END,f"\n---------------------------------------")        


                self.txtarea.insert(END,f"\n Total Bill\t\t\t    {self.Total_Bill}")
                self.txtarea.insert(END,f"\n---------------------------------------")        
                self.txtarea.insert(END,f"\n\t   THANKYOU VISIT AGAIN")  
                self.save_bill()      


         
        



    def total(self):
            self.orange_juice_price=(self.oj.get()*45)
            self.watermelon_juice_price=self.wj.get()*45
            self.Pineapple_juice_price=self.Pj.get()*45
            self.SweetLime_juice_price=self.Sj.get()*45
            self.Carrot_juice_price=self.cj.get()*45
            self.Pomgranate_juice_price=self.poj.get()*45
            self.total_juice_price=float(
                             self.orange_juice_price+
                             (self.watermelon_juice_price)+
                             (self.Pineapple_juice_price)+
                             (self.SweetLime_juice_price)+
                             (self.Carrot_juice_price)+
                             (self.Pomgranate_juice_price)
                             )
            self.Total_Juice_price.set("Rs. "+str(self.total_juice_price))
            self.j_tax=round((self.total_juice_price*0.05),2)
            self.Fries_meal_price=self.fi.get()*75
            self.Lunch_meal_price=self.li.get()*105
            self.Burger_meal_price=self.bi.get()*95
            self.Pizza_meal_price=self.pi.get()*125
            self.Cheese_Burger_meal_price=self.cbi.get()*170
            self.Cheese_HotDog_price=self.hi.get()*155
            
            self.total_item_price=float(
                             (self.Fries_meal_price)+
                             (self.Lunch_meal_price)+
                             (self.Burger_meal_price)+
                             (self.Pizza_meal_price)+
                             (self.Cheese_Burger_meal_price)+
                             (self.Cheese_HotDog_price)
                             )
            self.Total_Items_price.set("Rs. "+str(self.total_item_price))
            self.i_tax=round((self.total_item_price*0.05),2)
            self.Ice_Cream=self.id.get()*100
            self.Gulab_Jamun=self.gd.get()*110
            self.Brownie=self.sbd.get()*225
            self.Halwa1=self.hd.get()*130
            self.Fresh_Cream=self.fcd.get()*130
            self.CupCake=self.ccd.get()*255
            self.total_d_price=float(
                             (self.Ice_Cream)+
                             (self.Gulab_Jamun)+
                             (self.Brownie)+
                             (self.Halwa1)+
                             (self.CupCake)+
                             (self.Fresh_Cream)
                             )
            self.Total_Desserts_price.set("Rs. "+str(self.total_d_price))
            self.d_tax=round((self.total_d_price*0.05),2)
            self.without_taxes=self.total_juice_price+self.total_item_price+self.total_d_price

            self.Total_Bill=float(self.without_taxes+
                                  (self.without_taxes*0.025)+
                                  (self.without_taxes*0.025)
                                  )
            self.Juice_Tax.set("Rs. "+str(self.without_taxes)) 
            self.Items_Tax.set("Rs. "+str(self.without_taxes*0.025))
            self.Desserts_Tax.set("Rs. "+str(self.without_taxes*0.025)) 


        

    def __init__(self,root):
          
          self.root=root
          self.root.geometry("1370x700+0+0") #width,height, start index x , start index y
          
          self.root.title("Kapleshwar restaurnt")
          #self.propagate(0)
          bg_color='#074463'
          title=Label(self.root,text="Kapleshwar Restaurant",bd=12,relief=GROOVE,bg=bg_color,fg="white",font=("times new roman",30,"bold"),pady=2).pack(fill=X)
          #=================Variables=============================
          #=================Juices======================
          self.oj=IntVar()
          self.wj=IntVar()
          self.Pj=IntVar()
          self.Sj=IntVar()
          self.cj=IntVar()
          self.poj=IntVar()
          #=================Items===================   
          self.fi=IntVar()
          self.li=IntVar()
          self.bi=IntVar()
          self.pi=IntVar()
          self.cbi=IntVar()
          self.hi=IntVar()
          #=================Desserts=================
          self.id=IntVar()
          self.gd=IntVar()
          self.sbd=IntVar()
          self.hd=IntVar()
          self.ccd=IntVar()
          self.fcd=IntVar()

          #===============Totoal And Tax Price========
          self.Total_Juice_price=StringVar()
          self.Total_Items_price=StringVar()
          self.Total_Desserts_price=StringVar()

          self.Juice_Tax=StringVar()
          self.Items_Tax=StringVar()
          self.Desserts_Tax=StringVar()

          #===============Customer====================
          self.c_name=StringVar()
          self.c_phon=StringVar()
          self.bill_no=StringVar()
          x=random.randint(1000,9999)
          self.bill_no.set(str(x))
          self.search_bill=StringVar()
          #=====functions====
          





          
          #=================CUSTOMER DETAILS FRAME================
          F1=LabelFrame(self.root,text="Customer Details",bd=10,relief=GROOVE, font=(("times new roman"),15,"bold"),fg="gold",bg=bg_color)
          F1.place(x=0,y=80,relwidth=1)

          cname_label=Label(F1,text="Customer Name",bg=bg_color,fg="white",font=("times new roman",18,"bold")).grid(row=0,column=0,padx=5,pady=20)
          cname_txt=Entry(F1,width=20,bd=7,textvariable=self.c_name,relief=SUNKEN,font="arial 15").grid(row=0,column=1,pady=5,padx=10)

          cphn_label=Label(F1,text="Phone No.",bg=bg_color,fg="white",font=("times new roman",18,"bold")).grid(row=0,column=2,padx=5,pady=20)
          cphn_txt=Entry(F1,width=20,bd=7,textvariable=self.c_phon,relief=SUNKEN,font="arial 15").grid(row=0,column=3,pady=5,padx=10)

          cbill_label=Label(F1,text="Bill Number",bg=bg_color,fg="white",font=("times new roman",18,"bold")).grid(row=0,column=4,padx=5,pady=20)
          cbill_txt=Entry(F1,width=20,bd=7,textvariable=self.search_bill,relief=SUNKEN,font="arial 15").grid(row=0,column=5,pady=5,padx=10)
          
          bill_btn=Button(F1,text="search",command=self.find_bill,width=10,bd=7,font="arial 12 bold").grid(row=0,column=6,pady=10,padx=10)

          #==============Juices  FRAME

          F2=LabelFrame(self.root,text="Juices",bd=10,relief=GROOVE, font=(("times new roman"),15,"bold"),fg="gold",bg=bg_color)
          F2.place(x=5,y=170,width=375,height=380)

          orange_lbl=Label(F2,text="Orange Juice",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=0,column=0,padx=10,pady=10,sticky="w")
          orange_txt=Entry(F2,width=10,textvariable=self.oj,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=0,column=1,padx=10,pady=10)

          Watermelon_lbl=Label(F2,text="Watermelon Juice",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=1,column=0,padx=10,pady=10,sticky="w")
          Watermelon_txt=Entry(F2,width=10,textvariable=self.wj,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=1,column=1,padx=10,pady=10)

          pineappl_lbl=Label(F2,text="PineApple Juice",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=2,column=0,padx=10,pady=10,sticky="w")
          pineappl_txt=Entry(F2,width=10,textvariable=self.Pj,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=2,column=1,padx=10,pady=10)
          
          sweetlime=Label(F2,text="Sweet-Lime Juice",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=3,column=0,padx=10,pady=10,sticky="w")
          sweetlime_txt=Entry(F2,width=10,textvariable=self.Sj,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=3,column=1,padx=10,pady=10)
          
          Carrot_lbl=Label(F2,text="Carrot Juice",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=4,column=0,padx=10,pady=10,sticky="w")
          Carrot_txt=Entry(F2,width=10,textvariable=self.cj,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=4,column=1,padx=10,pady=10)

          Promgranate_lbl=Label(F2,text="Promgranate Juice",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=5,column=0,padx=10,pady=10,sticky="w")
          Progranate_txt=Entry(F2,width=10,textvariable=self.poj,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=5,column=1,padx=10,pady=10)

          #Continue from here video: https://youtu.be/rTpnyb3y5-w paused at 45:00

          #==============ITEMS FRAME

          F3=LabelFrame(self.root,text="Items",bd=10,relief=GROOVE, font=(("times new roman"),15,"bold"),fg="gold",bg=bg_color)
          F3.place(x=340,y=170,width=375,height=380)

          Fries_lbl=Label(F3,text="Fries Meal",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=0,column=0,padx=10,pady=10,sticky="w")
          Fries_txt=Entry(F3,width=10,textvariable=self.fi,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=0,column=1,padx=10,pady=10)

          Lunch_lbl=Label(F3,text="Lunch Meal",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=1,column=0,padx=10,pady=10,sticky="w")
          Lunchh_txt=Entry(F3,width=10,textvariable=self.li,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=1,column=1,padx=10,pady=10)

          Burger_lbl=Label(F3,text="Burger Meal",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=2,column=0,padx=10,pady=10,sticky="w")
          Burger_txt=Entry(F3,width=10,textvariable=self.bi,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=2,column=1,padx=10,pady=10)
          
          Pizza_lbl=Label(F3,text="Pizza Meal",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=3,column=0,padx=10,pady=10,sticky="w")
          Pizza_txt=Entry(F3,width=10,textvariable=self.pi,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=3,column=1,padx=10,pady=10)
          
          CBurger_lbl=Label(F3,text="Cheese Burger Meal",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=4,column=0,padx=10,pady=10,sticky="w")
          CBurger_txt=Entry(F3,width=10,textvariable=self.cbi,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=4,column=1,padx=10,pady=10)

          HotDog_lbl=Label(F3,text="Hotdog Meal",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=5,column=0,padx=10,pady=10,sticky="w")
          HotDog_txt=Entry(F3,width=10,textvariable=self.hi,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=5,column=1,padx=10,pady=10)
          

          #======================DESSERT ITEMS==========
          F4=LabelFrame(self.root,text="Desserts",bd=10,relief=GROOVE, font=(("times new roman"),15,"bold"),fg="gold",bg=bg_color)
          F4.place(x=690,y=170,width=375,height=380)

          IceCream_lbl=Label(F4,text="Ice Cream",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=0,column=0,padx=10,pady=10,sticky="w")
          IceCream_txt=Entry(F4,width=10,textvariable=self.id,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=0,column=1,padx=10,pady=10)

          Gulab_lbl=Label(F4,text="Gulab Jamun",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=1,column=0,padx=10,pady=10,sticky="w")
          Gulab_txt=Entry(F4,width=10,textvariable=self.gd,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=1,column=1,padx=10,pady=10)

          Halwa_lbl=Label(F4,text="Halwa",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=2,column=0,padx=10,pady=10,sticky="w")
          Halwa_txt=Entry(F4,width=10,textvariable=self.sbd,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=2,column=1,padx=10,pady=10)
          
          Browine_lbl=Label(F4,text="Sizzling Brownie",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=3,column=0,padx=10,pady=10,sticky="w")
          Browinw_txt=Entry(F4,width=10,textvariable=self.hd,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=3,column=1,padx=10,pady=10)
          
          Cupcake_lbl=Label(F4,text="Cup Cakes",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=4,column=0,padx=10,pady=10,sticky="w")
          Cupcake_txt=Entry(F4,width=10,textvariable=self.ccd,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=4,column=1,padx=10,pady=10)

          Fresh_Cream_lbl=Label(F4,text="Fresh Cream",font=("times new roman",16,"bold"),bg=bg_color,fg="lightgreen").grid(row=5,column=0,padx=10,pady=10,sticky="w")
          Fresh_Cream_txt=Entry(F4,width=10,textvariable=self.fcd,font=("times new roman",16,"bold"),bd=5,relief=SUNKEN).grid(row=5,column=1,padx=10,pady=10)
          
          #================Bill Area================
          F5=Frame(self.root,bd=10,relief=GROOVE)
          F5.place(x=1010,y=180,width=360,height=370)

          bill_label=Label(F5,text="Bill Area",font="arial 15 bold",bd=7,relief=GROOVE).pack(fill=X)
 
          scrol_y=Scrollbar(F5,orient=VERTICAL)
          self.txtarea=Text(F5,yscrollcommand=scrol_y.set)
          scrol_y.pack(side=RIGHT,fill=Y)
          scrol_y.config(command=self.txtarea.yview)
          self.txtarea.pack(fill=BOTH,expand=1)


          #====================Button Frame=================

          F6=LabelFrame(self.root,text="Bill Menu",bd=10,relief=GROOVE, font=(("times new roman"),15,"bold"),fg="gold",bg=bg_color)
          F6.place(x=0,y=560,relwidth=1,height=140)

          m1_lbl=Label(F6,text="Total Juice price",bg=bg_color,fg="white",font=("times new roman",14,"bold")).grid(row=0,column=0,padx=20,pady=1,sticky="w")
          m1_txt=Entry(F6,width=18,textvariable=self.Total_Juice_price,font="arial 10 bold",bd=7,relief=SUNKEN).grid(row=0,column=1,padx=10,pady=1)

          m2_lbl=Label(F6,text="Total Items price",bg=bg_color,fg="white",font=("times new roman",14,"bold")).grid(row=1,column=0,padx=20,pady=1,sticky="w")
          m2_txt=Entry(F6,width=18,textvariable=self.Total_Items_price,font="arial 10 bold",bd=7,relief=SUNKEN).grid(row=1,column=1,padx=10,pady=1)

          m3_lbl=Label(F6,text="Total Desserts price",bg=bg_color,fg="white",font=("times new roman",14,"bold")).grid(row=2,column=0,padx=20,pady=1,sticky="w")
          m3_txt=Entry(F6,width=18,textvariable=self.Total_Desserts_price,font="arial 10 bold",bd=7,relief=SUNKEN).grid(row=2,column=1,padx=10,pady=1)


          c1_lbl=Label(F6,text="Total Amount",bg=bg_color,fg="white",font=("times new roman",14,"bold")).grid(row=0,column=2,padx=20,pady=1,sticky="w")
          c1_txt=Entry(F6,width=18,textvariable=self.Juice_Tax,font="arial 10 bold",bd=7,relief=SUNKEN).grid(row=0,column=3,padx=10,pady=1)

          c2_lbl=Label(F6,text="CGST ",bg=bg_color,fg="white",font=("times new roman",14,"bold")).grid(row=1,column=2,padx=20,pady=1,sticky="w")
          c2_txt=Entry(F6,width=18,textvariable=self.Items_Tax,font="arial 10 bold",bd=7,relief=SUNKEN).grid(row=1,column=3,padx=10,pady=1)

          c3_lbl=Label(F6,text="SGST ",bg=bg_color,fg="white",font=("times new roman",14,"bold")).grid(row=2,column=2,padx=20,pady=1,sticky="w")
          c3_txt=Entry(F6,width=18,textvariable=self.Desserts_Tax,font="arial 10 bold",bd=7,relief=SUNKEN).grid(row=2,column=3,padx=10,pady=1)

          #==================Button Frame=============

          btn_F=Frame(F6,bd=7,relief=GROOVE)
          btn_F.place(x=750,width=580,height=105)

          total_btn=Button(btn_F,command=self.total,text="Total",bg="cadetblue",fg="white",pady=15,bd=2,width=10,font="arial 15 bold").grid(row=0,column=0,padx=5,pady=5)
          GBill_btn=Button(btn_F,command=self.bill_area,text="Generate Bill",bg="cadetblue",fg="white",pady=15,bd=2,width=10,font="arial 15 bold").grid(row=0,column=1,padx=5,pady=5)
          Clear_btn=Button(btn_F,text="Clear",command=self.clear_data,bg="cadetblue",fg="white",pady=15,bd=2,width=10,font="arial 15 bold").grid(row=0,column=2,padx=5,pady=5)
          Exit_btn=Button(btn_F,text="Exit",command=self.Exit_App,bg="cadetblue",fg="white",pady=15,bd=2,width=10,font="arial 15 bold").grid(row=0,column=3,padx=5,pady=5)
          self.welcome_bill()
    def save_bill(self):
        op=messagebox.askyesno("save bill","Do you want to save the bill?")
        if op>0:
                self.bill_data=self.txtarea.get('1.0',END)
                f1=open("bills/"+str(self.bill_no.get())+".txt","w")
                f1.write(self.bill_data)
                f1.close()
                messagebox.showinfo("saved",f"Bill No. {self.bill_no.get()} saved successfully!")
        else:
                return
    def find_bill(self):
        present="no"
        flag=1
        for i in os.listdir("bills/"):
                if i.split('.')[0]==self.search_bill.get():
                        present="yes"
                        flag=0
                        f1=open(f"bills/{i}","r")
                        self.txtarea.delete('1.0',END)
                        for d in f1:
                              self.txtarea.insert(END,d)
                        f1.close()
        if flag==1:
                messagebox.showerror("Error","Invalid Bill No.")       
    def clear_data(self):
          self.oj.set(0)
          self.wj.set(0)
          self.Pj.set(0)
          self.Sj.set(0)
          self.cj.set(0)
          self.poj.set(0)
          #=================Items===================   
          self.fi.set(0)
          self.li.set(0)
          self.bi.set(0)
          self.pi.set(0)
          self.cbi.set(0)
          self.hi.set(0)
          #=================Desserts=================
          self.id.set(0)
          self.gd.set(0)
          self.sbd.set(0)
          self.hd.set(0)
          self.ccd.set(0)
          self.fcd.set(0)

          #===============Totoal And Tax Price========
          self.Total_Juice_price.set('')
          self.Total_Items_price.set('')
          self.Total_Desserts_price.set('')

          self.Juice_Tax.set('')
          self.Items_Tax.set('')
          self.Desserts_Tax.set('')

          #===============Customer====================
          self.c_name.set('')
          self.c_phon.set('')
          self.bill_no.set('')
          x=random.randint(1000,9999)
          self.bill_no.set(str(x))
          self.search_bill.set('')
          self.welcome_bill()
    def Exit_App(self):
            op=messagebox.askyesno("Exit","Do you really want to exit")
            if op>0:
                    self.root.destroy()

root=Tk()
obj=Bill_App(root)
root.mainloop()